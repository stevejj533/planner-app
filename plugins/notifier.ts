import { Notif, Plugin } from '@nuxt/types'
import Vue from 'vue'

declare module 'vue/types/vue' {
  interface Vue {
    $notifier: {
      isAvailable: Boolean
      addNotif(payload: Notif): void
      removeNotif(notif: Notif): void
      toggleState(): void
      playSound(): void
    }
  }
}

declare module '@nuxt/types' {
  interface Notif {
    status: string
    content: string
  }
  interface NuxtAppOptions {
    $notifier: {
      isAvailable: Boolean
      addNotif(payload: Notif): void
      removeNotif(notif: Notif): void
      toggleState(): void
      playSound(): void
    }
  }
  interface Context {
    $notifier: {
      isAvailable: Boolean
      addNotif(payload: Notif): void
      removeNotif(notif: Notif): void
      toggleState(): void
      playSound(): void
    }
  }
}
const notifier: Plugin = ({ store }, inject) => {
  const globalNotifier = Vue.observable({
    isAvailable: true,
    async addNotif(payload: { status: ''; content: '' }): Promise<void> {
      if (!globalNotifier.isAvailable) return
      await store.dispatch('notify/addNotif', payload)
      setTimeout(async () => {
        await store.commit('notify/removeNotif')
      }, 3500)
    },
    async removeNotif(notif: { status: ''; content: '' }): Promise<void> {
      await store.dispatch('notify/removeNotif', notif)
    },
    toggleState(): void {
      globalNotifier.isAvailable = !globalNotifier.isAvailable
    },
    async playSound(): Promise<void> {
      return await new Audio('../assets/alert.mp3').play()
    },
  })
  inject('notifier', globalNotifier)
}
export default notifier
