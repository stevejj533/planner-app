import { getAccessorType, mutationTree, actionTree } from 'nuxt-typed-vuex'
import * as notify from './notify'

export const state = () => ({})
export const getters = {}
export const mutations = mutationTree(state, {})
export const actions = actionTree({ state, getters, mutations }, {})
export const accessorType = getAccessorType({
  actions,
  getters,
  mutations,
  state,
  modules: {
    notify,
  },
})
