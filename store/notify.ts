import { mutationTree, actionTree } from "nuxt-typed-vuex";
import { ActionTree } from "vuex";

type RootState = ReturnType<typeof state>;
type Notif = {
  status: "";
  content: "";
};
export const state = () => ({
  notifList: [] as Notif[],
});
export const mutations = mutationTree(state, {
  addNotif(state: RootState, payload: Notif): void {
    state.notifList.unshift(payload);
  },
  removeNotif(state: RootState, payload?: Notif): void {
    if (payload) {
      state.notifList.splice(state.notifList.indexOf(payload), 1);
    } else {
      state.notifList.splice(0, 1);
    }
  },
});

export const actions: ActionTree<RootState, RootState> = actionTree(
  { state, mutations },
  {
    addNotif({ commit, state }, payload: Notif): void {
      commit("addNotif", payload);
    },
    removeNotif({ commit, state }, payload: Notif): void {
      commit("removeNotif", payload);
    },
  }
);
